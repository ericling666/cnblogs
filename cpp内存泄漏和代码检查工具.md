- [cppcheck](#cppcheck)
  - [如何使用](#如何使用)
- [valgrind](#valgrind)
  - [如何使用](#如何使用-1)
- [Sanitizer](#sanitizer)
  - [如何使用](#如何使用-2)


# cppcheck
[cppcheck](https://cppcheck.sourceforge.io/)属于静态代码检查工具，能处理比较多的类型。


## 如何使用

```shell
# 分析目录
cppcheck <path to dir>

# 分析单个文件
cppcheck <path to cpp file>
```

# valgrind

[valgrind](https://valgrind.org/) 是一个动态分析工具，包含`memcheck`，`cachegrind`,`massif`等模块，可以用来检查内存使用，堆栈分析，CPU缓存操作等。

## 如何使用
```shell
# 使用memcheck工具，需保证<program>带有符号信息，即`-g`编译
valgrind <program>
```

对下面这段程序进行测试
```cpp
int main() {
  int *i = new int(9);
  std::cout << *i << std::endl;

  // delete i;
}
```
测试结果如图所示，这种级别的内存泄漏都能检查出来：

![img](https://img2023.cnblogs.com/blog/3365953/202404/3365953-20240403165141225-595322989.png)


# Sanitizer
[Sanitizers](https://github.com/google/sanitizers)是Google发起的开源工具集，包括`AddressSanitizer`, `MemorySanitizer`, `ThreadSanitizer`, `LeakSanitizer`。
`Sanitizers` 项目本是LLVM项目的一部分，但GNU也将该系列工具加入到了自家的GCC编译器中。

## 如何使用

| Sanitizer                          | 用法示例                                           | 说明                                                         |
| ---------------------------------- | -------------------------------------------------- | ------------------------------------------------------------ |
| AddressSanitizer                   | `gcc -fsanitize=address -g -o program program.c`   | 检测内存错误，如使用未初始化的内存、内存泄漏、缓冲区溢出等。 |
| ThreadSanitizer                    | `gcc -fsanitize=thread -g -o program program.c`    | 检测多线程程序中的数据竞争问题。                             |
| MemorySanitizer                    | `gcc -fsanitize=memory -g -o program program.c`    | 检测未初始化的内存访问。                                     |
| UndefinedBehaviorSanitizer (UBSan) | `gcc -fsanitize=undefined -g -o program program.c` | 检测C/C++程序中的未定义行为。例如，整数溢出、空指针引用等。  |
| LeakSanitizer (实验性)             | `gcc -fsanitize=leak -g -o program program.c`      | 检测内存泄漏。                                               |

搭配CMake时，使用方式如下：
```cmake
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address -fno-omit-frame-pointer")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=address")
```
同样对上面的cpp代码片段进行测试，效果如下：
![img](https://img2023.cnblogs.com/blog/3365953/202404/3365953-20240403171403867-1725831884.png)