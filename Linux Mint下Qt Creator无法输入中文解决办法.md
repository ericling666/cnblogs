> 注，本文所指的是linux中使用fcitx输入框架下，Qt程序输入中文的解决办法
> 如果是ibus输入框架，则不需要任何操作，可以直接输入中文
> 但是微信使用的是fcitx输入框架，且比较常用，故只能使用fcitx输入框架
> 
ubuntu下有对应的`fcitx-frontend-qt6`软件包，直接安装就能解决问题。

但是linuxmint只有基于qt5的，目前使用Qt online installer安装的Qt Creator是基于Qt6.6编译的
![img](https://img2023.cnblogs.com/blog/3365953/202403/3365953-20240326170511391-1278408427.png)
所以，只能自己编译对应的fcitx-frontend-qt6动态库，然后放到对应目录下

首先下载对应的源码 `git clone https://github.com/fcitx/fcitx-qt5.git`
然后修改CMakeLists.txt文件
![img](https://img2023.cnblogs.com/blog/3365953/202403/3365953-20240326171006799-255456283.png)

然后依次执行如下命令
export PATH=/home/eric/Qt/6.6.2/gcc_64/bin/:$PATH
export PATH=/home/eric/Qt/Tools/CMake/bin:$PATH
这里需保证Qt版本与Qt Creator中使用的Qt版本一致

然后对该软件包进行编译
```shell
mkdir build
cd build/
cmake -DQt6_DIR=/home/eric/Qt/6.6.2/gcc_64/lib/cmake/ ..
make -j4

```


最后把编译好的`libfcitxplatforminputcontextplugin-qt6.so`文件放到
`/home/eric/Qt/Tools/QtCreator/lib/Qt/plugins/platforminputcontexts`目录下即可

另外，编译的Qt程序一般也无法输入中文，只要是使用fcitx输入框架，且没有安装对应的`fcitx-frontend-qt5`或者`fcitx-frontend-qt6`软件包，都无法输入中文。

所以，可以编译好之后，可以执行 `sudo make install` 这样编译的Qt程序也可以输入中文