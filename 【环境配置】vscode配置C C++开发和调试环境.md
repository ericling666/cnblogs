> 按照本教程配置好之后，**不再需要写任何tasks.json和launch.json文件**，减轻记忆负担，即使是调试程序，也不用再写这些文件了，跟着做就能得到一个很好的集成开发环境

目录
- [msys2的安装与配置](#msys2的安装与配置)
- [vscode的配置](#vscode的配置)
- [测试环境](#测试环境)


## msys2的安装与配置

[msys2](https://www.msys2.org/)的安装比较简单，默认安装路径为`C:\msys64`，这个路径可以不用修改，因为vscode的`cmake tools`插件会自动扫描到这个路径，就不用手动添加这个工具链

**pacman的一些常用命令**

| 命令                       | 描述                     |
| -------------------------- | ------------------------ |
| `pacman -S <package_name>` | 从官方软件仓库安装软件包 |
| `pacman -Ss <search_term>` | 在软件仓库中搜索软件包   |
| `pacman -R <package_name>` | 从系统中移除软件包       |
| `pacman -Syu`              | 升级所有已安装的软件包   |
| `pacman -Qe`               | 列出已显式安装的软件包   |
| `pacman -Sc`               | 清理软件包缓存           |

**pacman配置清华镜像源**

```shell
sed -i "s#https\?://mirror.msys2.org/#https://mirrors.tuna.tsinghua.edu.cn/msys2/#g" /etc/pacman.d/mirrorlist*
```

> 该命令会把 /etc/pacman.d/mirrorlist* 文件中的msys2的源修改为清华源，下载速度更快

pacman 需要安装以下包
- base-devel
- mingw-w64-ucrt-x86_64-toolchain
- mingw-w64-ucrt-x86_64-cmake
- mingw-w64-ucrt-x86_64-clang-tools-extra

通过以下命令，安装上面这些包
```shell
pacman -S base-devel mingw-w64-ucrt-x86_64-toolchain mingw-w64-ucrt-x86_64-cmake mingw-w64-ucrt-x86_64-clang-tools-extra
```
> Note:
> - msys2的大部分包都是以mingw-w64+环境+包名命名的，所以很多时候，安装包之前，可以先用`pacman -Ss <package>`来获取完整的包名
> - 你可以选择把MSYS2 UCRT64集成到Windows Terminal和vscode的终端里，这一部分内容，可以参考[terminals](https://www.msys2.org/docs/terminals/)和[IDES and Text Editors](https://www.msys2.org/docs/ides-editors/)，这两个配置都是比较简单的，而且集成之后更加方便。

## vscode的配置

vscode的安装就不详细展开了，主要用到四个插件
- `C/C++`
- `clangd`
- `CMake`
- `CMake Tools`

除此之外，还需要把以下配置添加到vscode的配置文件中，
```json
"C_Cpp.autocomplete": "disabled",  // 禁用C/C++插件的提示功能，与clangd插件冲突
"clangd.detectExtensionConflicts": false,  // 禁用clangd插件检测冲突，因为C/C++插件的提示功能已经被禁用了
"clangd.path": "C:\\msys64\\ucrt64\\bin\\clangd.exe",// 指定clangd路径
"cmake.cmakePath": "C:/msys64/ucrt64/bin/cmake.exe", // 指定cmake路径
```

## 测试环境

接下来可以快速写一个helloworld项目进行测试，打开一个全新的目录，
ctrl+shit+p 调出面板,输入cmake，选择quickstart
![img](https://img2023.cnblogs.com/blog/3365953/202312/3365953-20231231170017839-1137725614.png)

输入项目名称

![img](https://img2023.cnblogs.com/blog/3365953/202312/3365953-20231231170039845-240338614.png)

选择类型 C 或者 C++项目

![img](https://img2023.cnblogs.com/blog/3365953/202312/3365953-20231231170121097-557963545.png)

开发可执行文件 还是 库文件

![img](https://img2023.cnblogs.com/blog/3365953/202312/3365953-20231231170145986-2045779117.png)

cmake tools自动为我们创建了main.cpp和CMakeLists.txt文件

![img](https://img2023.cnblogs.com/blog/3365953/202312/3365953-20231231170507259-1866141623.png)

可以直接双击下方状态栏运行和调试按钮

![img](https://img2023.cnblogs.com/blog/3365953/202401/3365953-20240122203007375-773023810.png)


运行：

![img](https://img2023.cnblogs.com/blog/3365953/202312/3365953-20231231170726756-1094574332.png)

调试：

![img](https://img2023.cnblogs.com/blog/3365953/202312/3365953-20231231170914681-101419311.png)

可以看到，非常的方便，再也不用去写tasks.json和launch.json文件了
