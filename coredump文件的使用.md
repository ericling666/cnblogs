coredump文件在调试程序时，能够快速将堆栈恢复到程序崩溃时的状态，对于代码调试很有帮助，尤其是对于哪些难以复现的bug。

在默认情况下，是不生成coredump文件的。
可以通过命令`ulimit -c`查看当前系统允许生成coredump文件的大小。默认情况下，返回`0`，表示不允许生成coredump文件。

通过命令`ulimit -c unlimited`设置对coredump文件的大小不设限制。需要注意的是，执行这条命令并不会永久保存这个设置，**需要将该命令写入`.bashrc`文件中，否则，仅在当前shell中生效**。

在默认情况下，生成的coredump文件在`/var/lib/systemd/coredump/`目录中，文件名的设定规则按照文件`/proc/sys/kernel/core_pattern`中的内容设置。生成的coredump文件是压缩过的格式，属于zst文件。

默认设置兼顾了coredump文件的大小，还能通过coredumpctl命令进行查看和调试，所以这里就采用默认配置。

coredumpctl的常用命令：
```shell
# 查看当前coredump文件列表
coredumpctl list

# 查看对应程序的coredump文件列表
coredumpctl list <executable>

# 调试程序，仅匹配最新的那个
coredumpctl debug <executable> <path to executable>

# 导出coredump文件
coredumpctl -o <coredump file name> dump <executable>
```

需要注意的是，**实际可执行文件为release模式下生成的coredump文件，是得不到太多可用信息的，可以在gdb中指定可执行文件路径，这个可执行文件可以携带调试信息，使用-g编译，但需要保证代码和release版本一致，那么这个coredump和可执行文件就是匹配的，可以在调试时获取到上下文信息**

可以通过命令`coredumpctl debug <executable> <path to executable>`指定带上下文信息的可执行文件，也可以使用命令`coredumpctl debug <executable>`对coredump进行查看，然后通过命令`file <path to executable>`加载带上下文信息的可执行文件。

还可以通过dump导出coredump文件，然后使用gdb调试。