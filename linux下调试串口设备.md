USB转串口常用`CH34x`芯片，该芯片有linux下的驱动。
在默认情况下，大部分linux发行版都包含了`CH34x`的驱动，唯一缺点就是版本比较久。

可以先插上开发板, 一般是挂载到`/dev/ttyCH341USB0`文件下，如果该文件不存在，有两种可能，一种是驱动版本太久，可以下载官方的驱动文件，然后编译安装。
官方驱动下载地址：[CH341SER_LINUX_ZIP](https://www.wch.cn/download/CH341SER_LINUX_ZIP.html)
安装方法见该压缩包中的`readme`文件。


另一种可能是驱动占用，`sudo dmesg | grep tty `可以查看挂载的设备，如果提示下面的信息：
```shell
[    0.425064] printk: console [tty0] enabled
[    1.288554] 00:05: ttyS0 at I/O 0x3f8 (irq = 4, base_baud = 115200) is a 16550A
[  980.188047] usb 2-2.1: FTDI USB Serial Device converter now attached to ttyUSB0
[  980.872665] usb 2-2.1: usbfs: interface 0 claimed by ftdi_sio while 'brltty' sets config #1
[  980.876265] ftdi_sio ttyUSB0: FTDI USB Serial Device converter now disconnected from ttyUSB0
```
出现**interface 0 claimed by ftdi_sio while 'brltty' sets config #1**时，就是`brltty`导致的，可以直接使用`sudo apt remove brltty`卸载该软件。
> 当然，也可以禁用该软件，这里不展开，我直接卸载掉该软件

串口调试软件

`sudo apt install minicom`可以安装`minicom`命令行程序，这个软件需要sudo执行，当然也可以看一下挂载的设备所在的用户组，然后将当前用户添加到对应的组
```shell
eric@eric-XPS-13-9360:~$ ll /dev/ttyCH341USB0 
crw-rw---- 1 root dialout 169, 0  5月  1 10:33 /dev/ttyCH341USB0
eric@eric-XPS-13-9360:~$ sudo usermod -aG dialout eric
```

这需要重启电脑生效，当然也可以直接用`sudo`打开`minicom`

首次使用`minicom`需要进行配置，执行`sudo minicom -s`(如果添加到了对应的用户组，则不需要使用sudo)进入配置页面，通过输入前面的选项修改对应的配置，主要修改两处地方：
选中`Serial port setup`
1. 修改串口设备，按照实际设备文件修改
2. `Hardware Flow Control`和`Software Flow Control`都是No
按回车键会回退到上一级，每次修改好之后按回车，如果不修改，则按esc键

然后执行`Save setup as dfl`会自动保存配置，之后就不用再进行修改了。然后按esc键退出修改界面，启动开发板的电源，就能看到uboot的信息了，等开机完毕，就可以输入命令了。

退出`minicom`可以先按ctrl+a，然后按z，在按Q退出。



