# Git 设置代理

clash开启系统代理时，浏览器可以访问github，但是git依然没有使用代理，可以使用以下命令，为git设置代理

```shell
git config --global http.proxy http:127.0.0.1:7890
git config --global https.proxy https:127.0.0.1:7890
```

当需要为wsl2中的git设置代理时，需要修改ip地址，因为这个clash是windows的服务进程，而wsl2访问windows服务，则需要用到windows的IP，这个ip可以通过以下命令获得

```shell
ping $(hostname).local
```

该命令的返回结果如下
![img](https://img2023.cnblogs.com/blog/3365953/202401/3365953-20240120094525571-1637969601.png)
从而可以知道windows的ip地址为172.18.208.1（相对于wsl2而言）
